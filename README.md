# o4a-offline-containers

Analysis containers to define production environments for GstLAL offline analyses in O4a.

## Veto definer file

Veto definer file downloaded 23-12-06 from head of [CAT1-HL-20231010-R1 branch](https://git.ligo.org/detchar/veto-definitions/-/blob/CAT1-HL-20231010-R1/cbc/O4/H1L1-HOFT_C00_O4_CBC.xml) ([permalink](https://git.ligo.org/detchar/veto-definitions/-/blob/df3e1e8ae43e5eae097896e960387b753ba16675/cbc/O4/H1L1-HOFT_C00_O4_CBC.xml)).

## containers

Containers are built automatically by the CI pipeline and pushed to the container registry at containers.ligo.org. For a non-writable container, pull with:
```bash
singularity build <build-name> docker://containers.ligo.org/gstlal/o4a-offline-containers:<analysis_tag>
```

For a writable container, pull with:
```bash
singularity build --fix-perms --sandbox <build-name> docker://containers.ligo.org/gstlal/o4a-offline-containers:<analysis_tag>
```

### Available analysis tags are:

**main**: **UNDER CONSTRUCTION**
