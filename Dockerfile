FROM containers.ligo.org/lscsoft/gstlal:offline_new-workflow

ARG gitlab_token
ARG gitlab_name

LABEL name "gstlal o4a edward offline analysis container" \
      date="2023-12-06" 

# ADD config files needed to launch an online analysis from scratch
ADD config.edward.yml offline-analysis/config.edward.yml
ADD H1L1-HOFT_C00_O4_CBC.xml offline-analysis/H1L1-HOFT_C00_O4_CBC.xml

# set up directories that will be needed
RUN mkdir -p offline-analysis/input_data/bank/
RUN mkdir -p offline-analysis/input_data/mass_model/

# ADD any patches we need to apply manually
#ADD patches/snglcoinc.patch patches/snglcoinc.patch

USER root

RUN mkdir -p src && cd src && \
    git clone https://$gitlab_name:$gitlab_token@git.ligo.org/lscsoft/lalsuite.git && \
    cd lalsuite && \
    git checkout lalsuite-v7.13 && \
    git apply ../../patches/snglcoinc.patch && \
    ./00boot && \
    ./configure --prefix=/usr --libdir=/usr/lib64 --enable-swig-python --disable-gcc-flags && \
    make && \
    make install -j10

ENTRYPOINT bash
